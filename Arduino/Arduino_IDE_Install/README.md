# Instalación de la IDE de Arduino

1. Descarga el instalador de la IDE (editor de programas de Arduino) desde [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software) y ejecutalo para que se lleve a cabo la instalación.

    ![](arduino_ide_download.jpg)

    ___

2. Ejecuta la IDE para luego especificar que trabajaremos con un Arduino Nano

    ![](arduino_ide_board.jpg)

    ___

3. Monta el Arduino Nano en la protoboard y conectalo al computador a través del cable USB.

    ![](arduino_usb.jpg)

    ___

4. Configura el tipo de procesador del Arduino Nano; en este caso  seleccionar ATMega328

    ![](arduino_ide_processor.jpg)

    ___

5. Configura la puerta en que se encuentra conectado el Arduino Nano

    ![](arduino_ide_port.jpg)

    ___

6. Carga el sketch (programa) "*Blink*" que apaga y enciende a intervalos regulares el LED (luz) interno del Arduino

    ![](arduino_ide_blink.jpg)

    ___

7. "*Compila*" y envia al Arduino Nano el programa Blink haciendo clic en el botón superior tal como se muestra en la imagen.

    ![](arduino_ide_blink_run.jpg)

    ___

Un sketch (programa) en Arduino se compone de dos partes:

1. setup(); en donde se coloca todo el código que da valores iniciales a las partes de un Arduino; y
2. loop(); en donde se coloca el código que interactua con el mundo físico.

En el programa "Blink"; la instrucción **`delay(1000)`** genera pausas de 1.000 milisegundos (1 segundo). Cambia dicho valor por otro (ej. 500) y vuelve a enviar el código al Arduino observando que sucede.

# Arduino Comic

Un interesante comic que te introducirá en el mundo de Arduino lo puedes descargar desde [Arduino PlayGround](http://playground.arduino.cc/uploads/Main/arduino_comic_es.pdf)

![](arduino_comic.jpg)

