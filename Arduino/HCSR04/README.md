# Sensor de distancia HC-SR04

1. Conecta el arduino y el sensor de distancia (HC-SR04) acorde a la siguiente imagen:

    ![](hcsr04.jpg)

    ___

2. Instala la librería que permitirá manejar el sensor de distancia. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

	![](Codigo.jpg)

    ```C++
    /*
     * Lectura de sensor ultrasonico HC-SR04
     */
     #include <Ultrasonic.h>
     Ultrasonic hcsr04(3, 2);

    void setup()
    {
      Serial.begin( 115200 );
    }

    void loop()
    {
      int distancia = hcsr04.distanceRead();
      Serial.print( "Distancia:" );
      Serial.print( distancia );
      Serial.print( " cm." );
      Serial.println();

      delay( 500 );
    }
    ```

    ___

4. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

5. Coloque un objeto delante del sensor y determina a que distancia de éste se encuentra.


