# Sensor de temperatura y humedad

1. Conecta el arduino y el sensor de temperatura y humedad (DHT22) acorde a una de las imágenes siguientes:

    ![](DHT22.jpg)

    ___

2. Instala la librería que permitirá manejar el sensor de temperatura y humedad. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

	![](Codigo.jpg)

    ```C++
    // incluimos la librería para el sensor dht22
    #include "DHT.h"

    // creamos una instancia para acceder al sensor en el pin D2 del arduino
    DHT dht( 2, DHT22 );

    void setup() {
      Serial.begin( 115200 );
      dht.begin();
    }

    void loop() {
      float dhtTemperatura = dht.readTemperature();
      float dhtHumedad = dht.readHumidity();
      yield();

      Serial.print( "Temperatura: " );
      Serial.print( dhtTemperatura, 1 );
      Serial.print( " °C" );
      Serial.print( ", Humedad:" );
      Serial.print( dhtHumedad, 1 );
      Serial.println( " %" );

      delay( 500 );
    }
    ```

    ___

4. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

5. Cubre el sensor con la mano para alterar la temperatura y observa el cambio en los valores detectados


