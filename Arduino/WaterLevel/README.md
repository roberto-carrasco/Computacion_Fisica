# Sensor de nivel de agua

1. Conecta el arduino y el sensor de nivel de agua acorde a la imagen siguiente:

    ![](WaterLevel.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

    ![](Codigo.jpg)

    ```C++
    /*
    * Lectura de sensor de nivel de agua
    */
    void setup()
    {
     Serial.begin( 115200 );
    }

    void loop()
    {
     int aVal = analogRead( A0 );
     Serial.print( "Data Analoga:" );
     Serial.print( aVal );
     Serial.println();

     delay( 500 );
    }
    ```

    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Introduce el extremo del sensor de nivel de agua en un vaso con agua. En diferentes instantes introdúcelo un poco más y observa los datos reportados.


