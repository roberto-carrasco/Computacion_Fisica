# Arduino Nano

![](ArduinoNano.jpg)
___

Lo primero es instalar la IDE de Arduino siguiendo las instrucciones que se encuentran [**++aquí++**](Arduino/Arduino_IDE_Install/README.md) y que te permitirán además realizar tu primer programa en Arduino.

## Sketchs

- [LED (Hola Mundo)](LED/README.md)
- [Botón Pulsador (Push Button)](PushButton/README.md)
- [Sensor de humedad en suelo](Hygrometer/README.md)
- [Sensor de cantidad de luz](LDR/README.md)
- [Sensor de gotas de lluvia](RainDrop/README.md)
- [Sensor de Temperatura y Humedad DHT22](DHT22/README.md)
- [Sensor de Presencia PIR](PIR/README.md)
- [Sensor de nivel de agua](WaterLevel/README.md)
- [Sensor de radiación ultravioleta GYML8511](GYML8511/README.md)
- [Sensor de Presión Barométrica GY-BME/P 280](GYBME280/README.md)
- [Sensor de Presión Barométrica GY-68/BMP180](GY68/README.md)
- [Sensor de Distancia HC-SR04](HCSR04/README.md)
- [Acelerómetro + Giróscopo MPU6050/GY521](MPU6050/README.md)
- [Lector de tarjetas RFID - MFRC522](MFRC522/README.md)
- [Mini Joystick](Joystick/README.md)
- [Matriz de 8x8 LEDs](Matrix8x8/README.md)
- [Pantalla LCD](LCD1602A/README.md)
- [Pantalla OLED](Oled/README.md)
- [LED RGB WS2812B](WS2812B/README.md)
