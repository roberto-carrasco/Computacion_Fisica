# Sensor de humedad en suelo

1. Conecta el arduino y el sensor de humedad en suelo acorde a una de las imágenes siguientes:

    ![](Hygrometer.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

    ![](Codigo.jpg)

    ```C++
    /*
     * Lectura de sensor de humedad en suelo (hygrometer)
     */
    void setup()
    {
      Serial.begin( 115200 );
    }

    void loop()
    {
      int aVal = analogRead( A0 );
      Serial.print( "Data Analoga:" );
      Serial.print( aVal );
      Serial.println();

      delay( 500 );
    }
    ```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Inserta el sensor en algún macetero y compara los datos al agregar agua a dicho macetero


