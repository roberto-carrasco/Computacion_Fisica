# Matriz de 8x8 LEDs

1. Conecta el arduino y la matriz de 8x8 Leds acorde a la imagen siguiente:

    ![](Matrix8x8.jpg)

    ___

2. Instala la librería que permitirá manejar la matriz de 8x8 LEDs. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

	![](Codigo.jpg)

    ```C++
    // incluimos la libreria para manejar la matriz
    #include "LedControl.h"

    // pin 12 conectado a DIN
    // pin 10 conectado a CLK
    // pin 11 conectado a DS
    // 1 unico MAX7219
    LedControl lc=LedControl( 12, 10, 11, 1);

    void setup() {
      // inicializamos la matriz
      lc.shutdown( 0, false );
      lc.setIntensity( 0, 8 );
      lc.clearDisplay( 0 );
    }

    void loop() {
      int x, y;

      for( x=0; x<1; x++ ){
        for( y=0; y<8; y++ ){
          lc.setLed( 0, x, y, HIGH );
          delay( 100 );
          lc.setLed( 0, x, y, LOW );
          delay( 100 );
        }
      }
    }
	```

    ___

4. Experimenta con los valores de x e y en la función ```loop()```. En el caso de la función ```setLed()``` el primer parámetro corresponde a la primera matriz de LEDs (en el ejemplo tenemos sólo una), los dos siguientes parámetros corresponden a las coordenadas del LED a manipular, y el último al valor de apagado o encendido de ese LED.


