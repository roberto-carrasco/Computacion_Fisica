# Botón Pulsador

1. Conecta el arduino, el botón pulsador y la resistencia acorde a la imagen siguiente:

    ![](PushButton.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

    ![](Codigo.jpg)

    ```C++
    /*
     * Uso de boton pulsador
     */

    boolean presionado = false;

    void setup() {
      Serial.begin( 115200 );
      pinMode( 2, INPUT );

    }

    void loop() {
      int p = digitalRead( 2 );
      if( p == HIGH && !presionado ){
        Serial.println( 1 );
        presionado = true;
      }
      else if( p == LOW && presionado ){
        Serial.println( 0 );
        presionado = false;
      }
    }
    ```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Prueba presionando varias veces el botón. Luego agrega un LED tal que se encienda y apague según presiones el botón.


