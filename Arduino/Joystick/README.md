# Mini Joystick

1. Conecta el arduino y el joystick acorde a la imagen siguiente:

    ![](Joystick.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

    ![](Codigo.jpg)

    ```C++
    /*
     * Lectura de joystick
     */
    void setup()
    {
      Serial.begin( 115200 );
    }

    void loop()
    {
      int joyX = analogRead( A1 );
      int joyY = analogRead( A0 );
      int button = digitalRead( 2 );
      Serial.print( "Joystic X:" );
      Serial.print( joyX );
      Serial.print( ", Y:" );
      Serial.print( joyY );
      Serial.print( ", Boton:" );
      Serial.print( button );
      Serial.println();

      delay( 500 );
    }
    ```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Mueve el joystick y presionalo en la parte superior para que puedas comprender los valores que entrega


