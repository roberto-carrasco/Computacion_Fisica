# LED RGB WS2812B

1. Conecta el arduino y el LED RGB WS2812B acorde a la siguiente imagen (si es preciso soldar 3 pines al LED):

    ![](ws2812b.jpg)

    ___

2. Instala la librería que permitirá manejar el LED RGB WS2812B. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (Arduino Nano)

	![](Codigo.jpg)

    ```C++
    /*
     * Uso de led RGB WS2801
     */
    #include <NeoPixelBus.h>

    const uint16_t PixelCount = 1;
    const uint8_t PixelPin = 2;

    NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip( PixelCount, PixelPin );

    #define colorSaturation 128
    RgbColor red( colorSaturation, 0, 0 );
    RgbColor green( 0, colorSaturation, 0 );
    RgbColor blue( 0, 0, colorSaturation );
    RgbColor white( colorSaturation );
    RgbColor black( 0 );

    void setup()
    {
      Serial.begin( 115200 );
      strip.Begin();
      strip.Show();
    }

    void loop()
    {
      strip.SetPixelColor(0, red);
      strip.Show();
      delay( 500 );
      strip.SetPixelColor(0, green );
      strip.Show();
      delay( 500 );
      strip.SetPixelColor(0, blue);
      strip.Show();
      delay( 500 );
      strip.SetPixelColor(0, black);
      strip.Show();
      delay( 500 );
    }
    ```

    ___

4. Modifica las líneas con la instrucción SetPixelColor para generar diferentes colores.


