# Incursiones en Computación Física

Ejemplos para experimentar con prototipos simples de interacción con el mundo físico utilizando controladores del tipo Arduino o los derivados de ESP8266.


## Arduino Nano

![](Arduino/ArduinoNano.jpg)[![](go.jpg)](Arduino/README.md)


## NodeMCU/ESP8266

![](NodeMCU/NodeMCU.jpg)[![](go.jpg)](NodeMCU/README.md)
