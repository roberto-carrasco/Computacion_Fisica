# Mini Joystick

1. Conecta el NodeMCU, el conversor Analogo/Digital (ADS1115) y el joystick acorde a la imagen siguiente:

    ![](Joystick.jpg)

    ___

2. Instala la librería que permitirá utilizar el conversor Análogo Digital para dotar de 4 entradas analógicas adicionales al controlador. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    /*
     * Lectura de joystick
     * Requiere de un conversor analogo digital extra
     */

     #include <Adafruit_ADS1015.h>

    Adafruit_ADS1115 ads;  // version de 16 bits

    void setup()
    {
      Serial.begin( 115200 );

      // 2/3x gain +/- 6.144V  1 bit = 0.1875mV
      ads.setGain(GAIN_TWOTHIRDS);  
      ads.begin();

      pinMode( D3, INPUT );
    }

    void loop()
    {
      // el boton
      int button = digitalRead( D3 );

      // valores de posicion
      int joyX = ads.readADC_SingleEnded(1);
      int joyY = ads.readADC_SingleEnded(0);

      // valores de posicion como voltaje
      float vJoyX = 6.114*joyX/32768.0;
      float vJoyY = 6.114*joyY/32768.0;

      Serial.print( "Joystick X:" );
      Serial.print( joyX );
      Serial.print( " (" );
      Serial.print( vJoyX,3 );
      Serial.print( "), Y:" );
      Serial.print( joyY );
      Serial.print( " (" );
      Serial.print( vJoyY,3 );
      Serial.print( "), Boton:" );
      Serial.print( button );
      Serial.println();

      delay( 500 );
    }
    ```
    ___

4. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

5. Mueve el joystick y presionalo en la parte superior para que puedas comprender los valores que entrega


