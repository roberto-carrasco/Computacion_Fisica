# Configuración de la IDE de Arduino para NodeMCU (ESP8266)

1. Ejecuta la IDE de Arduino y selecciona la opción de "Preferencias".

    ![](ide_go_preferences.jpg)

    ___

2. Agrega la URL ```"http://arduino.esp8266.com/stable/package_esp8266com_index.json"``` en la entrada que se muestra en la imagen. Esto agregará varios controladores basados en ESP8266 para ser programados desde la IDE

    ![](ide_preferences.jpg)

    ___

3. Ingresa al Gestor de Tarjetas ta como se muestra en la imagen

    ![](ide_go_boardsmanager.jpg)

    ___

4. Busca por "NodeMCU" e instala la opción que se presentará

    ![](ide_boardsmanager.jpg)

    ___

5. Monta el NodeMCU en la protoboard y conectalo al computador a través del cable USB.

    ![](NodeMCU_usb.jpg)

    ___

6. Selecciona la tarjeta NodeMCU V1.0

    ![](ide_set_nodemcu.jpg)

    ___

7. Configura el resto de los parámetros para el NodeMCU

    ![](ide_set_params.jpg)

    ___

6. Carga el sketch (programa) "*Blink*" que apaga y enciende a intervalos regulares el LED (luz) interno del Arduino

    ![](arduino_ide_blink.jpg)

    ___

7. "*Compila*" y envia al NodeMCU el programa Blink haciendo clic en el botón superior tal como se muestra en la imagen.

    ![](arduino_ide_blink_run.jpg)

    ___

Un sketch (programa) en Arduino se compone de dos partes:

1. setup(); en donde se coloca todo el código que da valores iniciales a las partes de un Arduino; y
2. loop(); en donde se coloca el código que interactua con el mundo físico.

En el programa "Blink"; la instrucción **`delay(1000)`** genera pausas de 1.000 milisegundos (1 segundo). Cambia dicho valor por otro (ej. 500) y vuelve a enviar el código al Arduino observando que sucede.

# Arduino Comic

Un interesante comic que te introducirá en el mundo de Arduino lo puedes descargar desde [Arduino PlayGround](http://playground.arduino.cc/uploads/Main/arduino_comic_es.pdf)

![](arduino_comic.jpg)

