# Controlar LED remotamente utilizando el protocolo MQTT

1. Instala la librería ```PubSubClient``` que te permitirá interactuar con el controlador a través de la WiFi utilizando el protocolo MQTT

    ![](PubSubClient1.jpg)
    ![](PubSubClient2.jpg)

    ___

2. Conecta el NodeMCU y el LED acorde a la imagen siguiente:

    ![](LED.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    #include <Arduino.h>
    #include <Wire.h>
    #include <ESP8266WiFi.h>
    #include <PubSubClient.h>
    #include <ArduinoJson.h>

    // objetos para la WiFi y el broker MQTT
    WiFiClient wiFiClient;
    PubSubClient mqtt( wiFiClient );

    // datos de la WiFi
    #define WIFI_SSID       "Coloque aquí el nombre de la WiFi"
    #define WIFI_PASS       "Coloque aquí la clave de la WiFi"

    // datos del broker MQTT
    #define MQTT_SERVER       "test.mosquitto.org"
    #define MQTT_PORT         1883
    #define MQTT_TOPIC_LED    "rcr/demo/led"
    String  MQTT_CLIENTID     = "Node_" + String( ESP.getChipId() );

    #define PIN_LED  D2

    void setup() {
      Serial.begin( 115200 );
      Serial.println();
      Serial.println();

      // configura los pines
      pinMode( PIN_LED, OUTPUT );
      digitalWrite( PIN_LED, 0 );

      // inicia conexion a la WiFi
      WiFi.setAutoConnect( true );
      WiFi.mode( WIFI_STA );
      WiFi.begin( WIFI_SSID, WIFI_PASS );
      Serial.print( "Verificando WiFi ." );
      unsigned long start = millis();
      while( 1 ){
        Serial.print( "." );
        if(  WiFi.status() == WL_CONNECTED ){
          Serial.println( " : Conectado" );
          break;
        }
        delay( 500 );
      }

      // establece broker MQTT
      mqtt.setServer( MQTT_SERVER, MQTT_PORT );
      mqtt.setCallback( doReceiveMessage );
      mqtt.connect( MQTT_CLIENTID.c_str() );
      mqtt.subscribe( MQTT_TOPIC_LED, 0 );
    }

    void loop() {
      mqtt.loop();
    }

    void doReceiveMessage( char *topic, byte *payload, unsigned int len  ){
      Serial.write( payload, len );
      Serial.println();
      if( payload[0] == '0' )
        digitalWrite( PIN_LED,  0 );
      else if( payload[0] == '1' )
        digitalWrite( PIN_LED,  1 );
      }
    ```
    ___

4. Ingresa los datos de tu WiFi en las líneas

    ```C++
    // datos de la WiFi
    #define WIFI_SSID       "Coloque aquí el nombre de la WiFi"
    #define WIFI_PASS       "Coloque aquí la clave de la WiFi"
    ```
    ___

5. Modifica los datos del servidor MQTT, la puerta en la que escucha y el topico del cual recibiremos la orden de encendido o de apagado (1 o 0) del LED. Utilizaremos la App llamada MQTT Dash (Android) para controlar el LED

    ```C++
    // datos del broker MQTT
    #define MQTT_SERVER       "test.mosquitto.org"
    #define MQTT_PORT         1883
    #define MQTT_TOPIC_LED    "rcr/demo/led"
    ```
    ___

6. Puedes modificar la función ```doReceiveMessage()``` para realizar otras acciones cada vez que recibas un mensaje

    ```C++
    void doReceiveMessage( char *topic, byte *payload, unsigned int len  ){
      Serial.write( payload, len );
      Serial.println();
      if( payload[0] == '0' )
        digitalWrite( PIN_LED,  0 );
      else if( payload[0] == '1' )
        digitalWrite( PIN_LED,  1 );
      }
    ```
    ___

7. Instala la App MQTT Dash (u otra para MQTT) desde el Play Store de Android

    ![](MQTTDash01.jpg) ![](MQTTDash02.jpg)
	___

8. Ejecútala y crea una entrada para el servidor MQTT

    ![](MQTTDash03.jpg) ![](MQTTDash04.jpg) ![](MQTTDash05.jpg)
    ___

9. Haz un tap en la entrada recién creada y agrega un botón para controlar el LED

    ![](MQTTDash06.jpg) ![](MQTTDash07.jpg) ![](MQTTDash08.jpg)
    ___

10. Ahora haz un tap sbre el botón para enviar el valor 1 o 0 hacia el controlador indicando así el encender o apagar el LED

    ![](MQTTDash09.jpg) ![](MQTTDash10.jpg)
    ___

