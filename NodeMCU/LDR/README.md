# Sensor de cantidad de luz

1. Conecta el NodeMCU y el sensor de cantidad de luz (LDR) acorde a la imagen siguiente:

    ![](LDR.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    /*
     * Lectura de sensor de cantidad de luz (LDR)
     */
    void setup()
    {
      Serial.begin( 115200 );
    }

    void loop()
    {
      int aVal = analogRead( A0 );
      Serial.print( "Data Analoga:" );
      Serial.print( aVal );
      Serial.println();

     delay( 500 );
    }
    ```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Cubre el sensor con la mano para limitar la cantidad de luz que recibe y observa el cambio en los valores detectados


