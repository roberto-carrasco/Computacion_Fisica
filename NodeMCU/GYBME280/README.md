# Sensor de Presión Barométrica GY-BME/P 280

1. Conecta el NodeMCU y el sensor GY-BME/P 280 acorde a la imagen siguiente:

    ![](GYBME280.jpg)

    ___

2. Instala la librería que permitirá manejar el sensor BME/P 280. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    #include <Wire.h>
    #include <BME280I2C.h>

    BME280I2C bmp280;

    void setup() {
      Serial.begin( 115200 );
      while( !bmp280.begin() ){
        Serial.println("\nNo Encontrado" );
        delay( 1000 );
      }
    }

    void loop() {
      float temperatura, humedad, presion;
      bmp280.read( presion, temperatura, humedad, true, 0x00 );

      Serial.print( "Temperatura: ");
      Serial.print( temperatura );
      Serial.print( "°C" );
      Serial.print( ", Humedad: " );
      Serial.print( humedad );
      Serial.print( "%" );
      Serial.print( ", Presión: ");
      Serial.print( presion );
      Serial.println( "Pa" );

      delay( 1000 );
    }
    ```
    ___

4. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

5. Revisa los datos que se despliegan y compáralos con los que entrega alguna página de reporte de clima para tu localidad.