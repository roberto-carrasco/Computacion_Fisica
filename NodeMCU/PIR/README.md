# Sensor de presencia PIR

1. Conecta el NodeMCU y el sensor de presencia (PIR) acorde a la imagen siguiente:

    ![](PIR.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    // pin en donde se conecta el sensor
    #define pirPin  D2

    void setup() {
      Serial.begin( 115200 );
      pinMode( pirPin, INPUT );
    }

    void loop() {
      int val = digitalRead( pirPin );
      Serial.print( "Dato Sensor: " );
      Serial.println( val );
    }
	```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Pasa la mano frente al sensor y observa el dato que muestra. Quita la mano y en pocos segundos el valor reportado cambiará.


