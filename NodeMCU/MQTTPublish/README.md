# Enviar datos de humedad y temperatura con protocolo MQTT

1. Copia el siguiente código en la IDE de Arduino

    ![](Codigo.jpg)

    ```C++
    #include <Arduino.h>

    //---
    #define WIFI_SSID       "your wifi SID"
    #define WIFI_PASS       "your wifi password"
    #define MQTT_SERVER     "test.mosquitto.org"
    #define MQTT_PORT       1883
    #define MQTT_TOPIC      "rcr/dht22"
    String  MQTT_CLIENTID = "Node_" + String( ESP.getChipId() );
    #define DELAY           2000

	//---
    #include <ESP8266WiFi.h>
    WiFiClient net;

    #include <PubSubClient.h>
    PubSubClient mqtt( net );

    //---
    #include <ArduinoJson.h>
    StaticJsonDocument<256> jsonDoc;

    //---
    #include "DHT.h"
    DHT dht( D5, DHT22 );

    //---
    void setup() {
      Serial.begin( 115200 );
      while ( !Serial ) delay( 50 );
      Serial.println();
      Serial.println();

      WiFi.mode( WIFI_STA );
      WiFi.setAutoConnect( true );
      WiFi.begin( WIFI_SSID, WIFI_PASS );

      mqtt.setServer( MQTT_SERVER, MQTT_PORT );

      while( !wifiReconnect() || !mqttReconnect() );
    }

    bool wifiReconnect(){
      if( WiFi.status() == WL_CONNECTED )
        return true;

      Serial.print( "Conectando a la WiFi: ." );
      for( int i=0; i<10; i++ ){
        if( WiFi.status() == WL_CONNECTED ){
          Serial.println( " Ok" );
          return true;
        }

        Serial.print( "." );
        delay( 500 );
      }
      Serial.println( "Timeout" );
      return false;
    }

    bool mqttReconnect (){
      if( mqtt.connected() )
        return true;

      Serial.print( "Conectando a MQTT: ." );
      for( int i=0; i<10; i++ ){
        if( mqtt.connect( MQTT_CLIENTID.c_str() ) ){
          mqtt.subscribe( MQTT_TOPIC, 0 );
          Serial.println( " Ok" );
          return true;
        }

        Serial.print( "." );
        delay( 500 );
      }
      Serial.println( "Timeout" );
      return false;
    }

    void loop() {
        while( !wifiReconnect() || !mqttReconnect() ){
          mqtt.loop();
          yield();
        }

      float temp = dht.readTemperature();
      float hum = dht.readHumidity();
      yield();

      jsonDoc["temp"] = temp;
      jsonDoc["hum"] = hum;

      char buffer[256];
      serializeJson( jsonDoc, buffer );

      Serial.println( buffer );
      mqtt.publish( MQTT_TOPIC, buffer );
      Serial.flush();

      mqtt.loop();
      delay( DELAY );
    }

    ```
    ___

2. Ingresa los datos de tu WiFi en las líneas

    ```C++
    #define WIFI_SSID       "your wifi SID"
    #define WIFI_PASS       "your wifi password"
    ```
    ___

3. Coloca los datos para el servidor MQTT en la zona del código que dice:

    ```C++
    #define MQTT_SERVER     "test.mosquitto.org"
    #define MQTT_PORT       1883
    #define MQTT_TOPIC      "rcr/dht22"
    ```
    ___

4. Envía el codigo al controlador (NodeMCU) y revisa la data utilizando el monitor serial

    ![](SerialMonitor.jpg)

    ___

5. Instala en tu Android la aplicación MQTT Dash y configurala acorde a las imágenes siguientes:

    ![](MqttDash.jpg)

    ___





