# Matriz de 8x8 LEDs

1. Conecta el NodeMCU y la matriz de 8x8 Leds acorde a la imagen siguiente:

    ![](Matrix8x8.jpg)

    ___

2. Instala la librería que permitirá manejar la matriz de 8x8 LEDs. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

	![](Codigo.jpg)

    ```C++
    // incluimos la libreria para manejar la matriz
    #include <LEDMatrixDriver.hpp>

    #define PIN_DIN D7
    #define PIN_CS  D4
    #define PIN_CLK D5
    LEDMatrixDriver lmd( 1, PIN_CS );

    void setup() {
      // inicializamos la matriz
      lmd.setEnabled( true );
      lmd.setIntensity( 2 );
    }

    void loop() {
      int x, y;

      lmd.clear();
      for( y=0; y<8; y++ ){
        for( x=0; x<8; x++ ){
          lmd.setPixel( y, x, HIGH );
          lmd.display();
          delay( 100 );
        }
      }
    }
    ```

    ___

4. Experimenta con los valores de x e y en la función ```loop()```. En el caso de la función ```setPixel()``` los dos primeros parámetros corresponden a las coordenadas del LED a manipular, y el último al valor de apagado o encendido de ese LED.


