# WiFi Smart Car Kit

![](DoitCar.jpg)
____

Este es un kit bastante popular en [AliExpress](https://www.aliexpress.com/wholesale?SearchText=wifi+smart+car) que utiliza el NodeMCU como controlador. El código de control se encuentra desarrollado en LUA y lo puedes encontrar en [GitHub](https://github.com/SmartArduino/DoitCar).

## Conexiones
Lamentablemente no vienen las instrucciones para ensamblar el robot, pero si revisas las siguientes imágenes podrás montarlo sin mayores problemas teniendo presente:

- Los colores de los cables y en donde van conectados/soldados
- Al soldar los cables a los motores debes retirar el soporte plaśtico levantándolo un poco desde el centro y luego tirándolo hacia un costado
- Puse un jumper para alimentar los motores y el NodeMCU desde las pilas de manera simultánea


![](DesdeArriba.jpg)

![](DesdeAbajo.jpg)

## App en Google Play
![](GooglePlay.jpg)

Descarga la aplicación `Doit Car (AP Mode)`, enciende el robot y conéctate a la red WiFi que crea.

## Código para la IDE de Arduino

Si deseas intervenir el código te dejamos aquí una versión a ser trabajada directamente desde la IDE de Arduino

  ```C++
  #include <ESP8266WiFi.h>

  String MySSID = String( "NodeMCU-" ) + String( ESP.getChipId() );
  #define MyPASS  "1234567890"
  #define MyIP    IPAddress( 192, 168, 1, 1 )
  #define MyMask  IPAddress( 255, 255, 255, 0 )
  #define MyGw    MyIP
  #define MyPort  9003

  WiFiServer server( MyPort );

  byte ledState = 0;
  unsigned long lastBlink = 0;
  byte dataRecv[ 16 ];

  int speedA = 800, speedB = 800;
  void setup()
  {
    Serial.begin( 115200 );
    Serial.println();
    Serial.println();

    // LED
    pinMode( LED_BUILTIN, OUTPUT ); digitalWrite( LED_BUILTIN, LOW );

    // Motores, PWM 1Khz, Duty 1023
    analogWriteFreq( 1000 );
    analogWriteRange( 1023 );

    // Power motores A (izquierda) y B (derecha)
    pinMode( D1, OUTPUT ); analogWrite( D1, 0 );
    pinMode( D2, OUTPUT ); analogWrite( D2, 0 );

    // Control motores A y B; HIGH=Forward, LOW=Backward
    pinMode( D3, OUTPUT ); digitalWrite( D3, HIGH );
    pinMode( D4, OUTPUT ); digitalWrite( D4, HIGH );

    // Access Point
    WiFi.mode( WIFI_AP );
    WiFi.softAPConfig( MyIP, MyGw, MyMask );
    WiFi.softAP( MySSID.c_str(), MyPASS );

    // El server
    server.begin();
    Serial.println( "Robot Control Started" );
  }

  void loop()
  {
    // para indicar que estamos vivos
    unsigned long t = millis();
    if( t - lastBlink >= 1000L ){
      ledState = ledState ? LOW : HIGH;
      digitalWrite( LED_BUILTIN, ledState );
      lastBlink = t;
    }

    WiFiClient client = server.available();
    if( !client ) return;

    Serial.println();
    Serial.println( "Cliente Conectado" );
    while( client.connected() )
    {
      if( client.available() )
      {
        if( readFromClient( client, dataRecv, 3, 0 ) )
        {
          Serial.write( dataRecv, 3 );
          client.print( "ok\r\n" );
          switch( dataRecv[0] )
          {
            case '0':                 // Stop
              analogWrite( D1, 0 );
              analogWrite( D2, 0 );
              break;
            case '1':                 // Forward
              digitalWrite( D3, HIGH );
              digitalWrite( D4, HIGH );
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '2':                 // Backward
              digitalWrite( D3, LOW );
              digitalWrite( D4, LOW );
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '3':                 // Left
              digitalWrite( D3, LOW );
              digitalWrite( D4, HIGH );
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '4':                 // Right
              digitalWrite( D3, HIGH );
              digitalWrite( D4, LOW );
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '6':                 // A: Speed Up
              speedA += 50;
              if( speedA > 1023 ) speedA = 1023;
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '7':                 // A: Speed Down
              speedA -= 50;
              if( speedA < 0 ) speedA = 0;
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '8':                 // B: Speed Up
              speedB += 50;
              if( speedB > 1023 ) speedB = 1023;
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
            case '9':                 // B: Speed Down
              speedB -= 50;
              if( speedB < 0 ) speedB = 0;
              analogWrite( D1, speedA );
              analogWrite( D2, speedB );
              break;
          }
        }
      }
    }
    Serial.println();
    Serial.println( "Cliente Desconectado" );
  }

  int readFromClient( WiFiClient &client, byte *buff, int len, long timeout )
  {
    if( len <= 0 || timeout < 0 )
      return 0;

    int i = 0;
    unsigned long t = millis();

    while( true )
    {
      yield();
      if( !client.connected() ) return 0;

      if( timeout && ( millis() - t ) >= timeout ) break;

      int c = client.read();
      if( c < 0 ) continue;

      buff[ i++ ] = c;
      if( i >= len ) break;
    }
    return i;
  }
  ```

