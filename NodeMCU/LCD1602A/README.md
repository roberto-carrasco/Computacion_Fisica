# Pantalla LCD

1. Conecta el NodeMCU y la pantalla LCD acorde a la imagen siguiente:

    ![](LCD1602A.jpg)

    ___

2. Instala la librería que permitirá manejar la pantalla LCD. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

	![](Codigo.jpg)

    ```C++
    // las librerias requeridas
    #include <Wire.h>
    #include <LiquidCrystal_I2C.h>

    // creamos una instancia del LCD en la direccion 0x27
    // de 16 filas y 2 columnas
    LiquidCrystal_I2C lcd( 0x27, 16, 2 );

    void setup()
    {
      // inicializamos el LCD
      lcd.init();
      lcd.backlight();
    }

    void loop()
    {
      lcd.clear();
      lcd.setCursor( 0, 0 ); // (columna,fila)
      lcd.print( "Hola Mundo!!!" );

      int i;
      for( i=0; i<10; i++ ){
        lcd.scrollDisplayRight();
        delay( 500 );
      }
    }
    ```

    ___

4. Experimenta mostrando texto en diferentes filas y columnas. El display que utilizamos es de 16 columnas y dos filas.


