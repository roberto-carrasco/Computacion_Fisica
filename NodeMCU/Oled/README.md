# Pantalla OLED

1. Conecta el NodeMCU y la pantalla OLED acorde a la siguiente imagen:

    ![](oled.jpg)

    ___

2. Instala la librería que permitirá manejar la pantalla OLED. Para ello sigue la secuencia que se muestra en las siguientes imágenes:

    ![](Library_1.jpg)

    ![](Library_2.jpg)

    ___

3. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

	![](Codigo.jpg)

    ```C++
    /*
     * Uso de pantalla OLED SD1306 generica
     */
    #include <U8x8lib.h>
    U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // OLEDs without Reset of the Display

    void setup()
    {
      Serial.begin( 115200 );
      u8x8.begin();
      u8x8.setPowerSave(0);
    }

    void loop()
    {
      u8x8.setFont( u8x8_font_chroma48medium8_r );
      u8x8.drawString( 0, 0,"Hola" );
      u8x8.drawString( 0, 1,"a" );
      u8x8.drawString( 0, 2,"Todos!!!" );
      u8x8.refreshDisplay();
      delay( 2000 );
    }
    ```

    ___

4. Modifica las líneas con la instrucción drawString para agregar tus mensajes en diferentes posiciones de la pantalla OLED.


