# NodeMCU/ESP8266

![](NodeMCU.jpg)
___

En el caso de los NodeMCU (tipo [**++AMICA++**](https://frightanic.com/iot/comparison-of-esp8266-nodemcu-development-boards/)) también utilizaremos la IDE de Arduino, pero una vez [**++instalada++**](Arduino/Arduino_IDE_Install/README.md) deberás realizar los ajustes que se muestran [**++aquí++**](NodeMCU/Arduino_IDE_Config/README.md).

Importante: la versión 2.3.0 del código ESP8266 para la IDE de Arduino presente problemas de precisión en la lectura análoga en A0 ([#issue 2672](https://github.com/esp8266/Arduino/issues/2672)). Si es relevante la precisión, se recomienda instalar desde [github](https://github.com/esp8266/Arduino).

## Sketchs

- [LED (Hola Mundo)](LED/README.md)
- [Botón Pulsador (Push Button)](PushButton/README.md)
- [Sensor de humedad en suelo](Hygrometer/README.md)
- [Sensor de cantidad de luz](LDR/README.md)
- [Sensor de gotas de lluvia](RainDrop/README.md)
- [Sensor de Temperatura y Humedad DHT22](DHT22/README.md)
- [Sensor de Presencia PIR](PIR/README.md)
- [Sensor de nivel de agua](WaterLevel/README.md)
- [Sensor de radiación ultravioleta GYML8511](GYML8511/README.md)
- [Sensor de Presión Barométrica GY-BME/P 280](GYBME280/README.md)
- Sensor de Presión Barométrica GY-68/BMP180
- [Sensor de Distancia HC-SR04](HCSR04/README.md)
- [Acelerómetro + Giróscopo MPU6050/GY521](MPU6050/README.md)
- Lector de tarjetas RFID - MFRC522
- [Mini Joystick](Joystick/README.md)
- [Matriz de 8x8 LEDs](Matrix8x8/README.md)
- [Pantalla LCD](LCD1602A/README.md)
- [Pantalla OLED](Oled/README.md)
- [LED RGB WS2812B](WS2812B/README.md)
- [Envío de datos remoto utilizando el protocolo MQTT](MQTTPublish/README.md)
- [Controlar LED remotamente utilizando el protocolo MQTT](MQTTSubscribe/README.md)

## WiFi Smart Car Kit

![](DoitCar/DoitCar.jpg)[![](go.jpg)](DoitCar/README.md)
