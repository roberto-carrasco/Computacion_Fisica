# Sensor de gotas de lluvia

1. Conecta el NodeMCU y el sensor de gotas de lluvia acorde a una de las imágenes siguientes:

    ![](RainDrop.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    /*
    * Lectura de sensor de gotas de lluvia
    */
    void setup()
    {
      Serial.begin( 115200 );
    }

    void loop()
    {
      int aVal = analogRead( A0 );
      Serial.print( "Data Analoga:" );
      Serial.print( aVal );
      Serial.println();

      delay( 500 );
    }
    ```

    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Deja caer pequeñas gotas de agua sobre el sensor y observa los datos reportados


