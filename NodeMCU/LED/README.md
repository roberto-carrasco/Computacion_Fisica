# LED (Hola Mundo)

1. Conecta el NodeMCU y el LED acorde a la imagen siguiente:

    ![](LED.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    void setup() {
      Serial.begin( 115200 );
      pinMode( D2, OUTPUT );

    }

    void loop() {
      digitalWrite( D2, HIGH );
      delay( 500 );

      digitalWrite( D2, LOW );
      delay( 500 );
    }
    ```
    ___

3. Modifica el código de la función loop() para que el LED se apague y encienda en diferentes secuencias


