# Acelerómetro + Giróscopo MPU6050/GY521

1. Conecta el NodeMCU y la MPU6050/GY521 acorde a la imagen siguiente:

    ![](MPU6050.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    /*
     * Lectura de acelerometro + giroscopo MPU6050/GY521
     * 
     * http://playground.arduino.cc/Main/MPU-6050
     */

    #include<Wire.h>

    #define MPU_ADDR  0x68                      // I2C address of the MPU-6050

    int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

    void setup(){
      Serial.begin( 115200 );

      Wire.begin();
      Wire.beginTransmission( MPU_ADDR );
      Wire.write( 0x6B );                       // PWR_MGMT_1 register
      Wire.write( 0 );                          // set to zero (wakes up the MPU-6050)
      Wire.endTransmission( true );
    }
    void loop(){
      Wire.beginTransmission( MPU_ADDR );
      Wire.write( 0x3B );                       // starting with register 0x3B (ACCEL_XOUT_H)
      Wire.endTransmission( false );
      Wire.requestFrom( MPU_ADDR, 14, true );   // request a total of 14 registers

      AcX = Wire.read()<<8 | Wire.read();         // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
      AcY = Wire.read()<<8 | Wire.read();       // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
      AcZ = Wire.read()<<8 | Wire.read();       // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      Tmp = Wire.read()<<8 | Wire.read();       // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
      GyX = Wire.read()<<8 | Wire.read();       // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
      GyY = Wire.read()<<8 | Wire.read();       // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      GyZ = Wire.read()<<8 | Wire.read();       // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

      Serial.print("AcX = "); Serial.print( AcX );
      Serial.print(" | AcY = "); Serial.print( AcY );
      Serial.print(" | AcZ = "); Serial.print( AcZ );
      Serial.print(" | Tmp = "); Serial.print( Tmp/340.00 + 36.53 ); //equation for temperature in degrees C from datasheet
      Serial.print(" | GyX = "); Serial.print( GyX );
      Serial.print(" | GyY = "); Serial.print( GyY );
      Serial.print(" | GyZ = "); Serial.println( GyZ );

      delay( 333 );
    }
    ```

    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Levanta la protoboard y muevela en diferentes direcciones observando los datos reportados.
