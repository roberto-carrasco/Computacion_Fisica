# Sensor de radiación UV

1. Conecta el NodeMCU y el sensor UV (GYML8511) acorde a la imagen siguiente:

    ![](GYML8511.jpg)

    ___

2. Copia el siguiente código en la IDE de Arduino y envíalo al microcontrolador (NodeMCU)

    ![](Codigo.jpg)

    ```C++
    /*
     * Sensor UV
     */
    void setup() {
      Serial.begin( 115200 );
    }

    void loop() {
      // leemos el ADC en A0
      int a0 = analogRead( A0 );
      float vA0 = 3.3/1024.0*a0;
      float uvA0 = mapfloat( vA0, 0.99, 2.9, 0.0, 15.0 );
      Serial.print( "A0: " );
      Serial.print( a0 );
      Serial.print( ", " );
      Serial.print( vA0 );
      Serial.print( "v, " );
      Serial.print( uvA0 );
      Serial.println( "mw/cm^2" );
     }

    //From: http://forum.arduino.cc/index.php?topic=3922.0
    float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
    {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
    ```
    ___

3. Deja visible la ventana en donde se despliegan los valores de las instrucciones `Serial.print()` haciendo clic en la lupa que se encuentra en la parte superior derecha de la IDE. Vertifica que en la zona inferior derecha aparezca `Newline` y `115200`.

    ![](SerialMonitor.jpg)

    ___

4. Intenta verificar los datos reportados y monitorealos en distintos lugares.


