# Envío de datos con protocolo MQTT

1. Copia el siguiente código en la IDE de Arduino

    ![](Codigo.jpg)

    ```C++
    // librerías mínimas requeridas
    #include <ESP8266WiFi.h>
    #include <PubSubClient.h>
    #include <Wire.h>

    // objetos para la WiFi y el broker MQTT
    WiFiClient wiFiClient;
    PubSubClient mqtt( wiFiClient );

    // datos de la WiFi
    #define WIFI_SSID       "Coloque aquí el nombre de la WiFi"
    #define WIFI_PASS       "Coloque aquí la clave de la WiFi"

    // datos del broker MQTT
    #define MQTT_SERVER     "test.mosquitto.org" 
    #define MQTT_PORT       1883
    String  MQTT_CLIENTID = "Node_" + String( ESP.getChipId() );

    // control del código
    #define MODO_MQTT       0       // 0: modo testing, 1: modo MQTT
    #define MS_SLEEP        5000    // milisegundos de espera entre ciclos
    #define DEEP_SLEEP      0       // 0: no usar DeepSleep, 1: usar DeepSleep

    // colocar aquí definiciones para su programa

    void setup()
    {
      // inicia monitor serial
      Serial.begin( 115200 );
      Serial.println();
      Serial.println();

      #if MODO_MQTT==1
      // inicia conexión a la WiFi
      WiFi.setAutoConnect( true );
      WiFi.mode( WIFI_STA );
      WiFi.begin( WIFI_SSID, WIFI_PASS );

      // establece broker MQTT
      mqtt.setServer( MQTT_SERVER, MQTT_PORT );
      #endif

      // colocar aquí inicialización de sensores

      // requerido para dar tiempo a los sensores
      delay( 500 );
    }

    void loop()
    {
      // algunas variables relevantes
      unsigned long start;
      char str[32];

      // lectura del sensor
      int data = analogRead( A0 );
      yield();

      // despliegue del dato leido
      Serial.print( "Data Analoga:" );
      Serial.print( data );
      Serial.println();
      yield();

      #if MODO_MQTT==0
      delay( MS_SLEEP );
      return;
      #endif

      // esperamos la conexión a la WiFi
      Serial.print( "Verificando WiFi ." );
      start = millis();
      while( ( millis() - start ) < 15UL * 1000UL ){
        Serial.print( "." );
        if(  WiFi.status() == WL_CONNECTED ){
          Serial.println( " : Conectado." );

          // conectamos con el broker MQTT
          Serial.println ( "Conectando a servidor MQTT" );
          if( mqtt.connect( MQTT_CLIENTID.c_str() ) ){
            Serial.println( "Publicando ..." );

            // publicamos la data
            dtostrf( data, 1, 2, str );
            mqtt.publish( "ipst/test/sensor1/A0", str );
          }
          break;
        } else {
          delay( 500 );
        }
      }

      Serial.println();
      Serial.println( "Reiniciando ..." );

      // repetimos el proceso con la pausa apropiada
      #if DEEP_SLEEP==0
      delay( MS_SLEEP );
      #else
      ESP.deepSleep( MS_SLEEP * 1000UL);
      #endif
    }
    ```
    ___

2. Ingresa los datos de tu WiFi en las líneas

    ```C++
    // datos de la WiFi
    #define WIFI_SSID       "Coloque aquí el nombre de la WiFi"
    #define WIFI_PASS       "Coloque aquí la clave de la WiFi"
    ```
    ___

3. Agrega las librerías y objetos de tus sensores en la zona del código que dice:

    ```C++
    // colocar aquí definiciones para su programa

    ```
    ___

4. De igual manera agrega dentro de la función ```setup()``` las inicializaciones requeridas por tus sensores en la zona del código que dice:

    ```C++
    // colocar aquí inicialización de sensores

    ```
    ___

5. Dentro de la función ```loop()``` agrega el código requerido para leer tus sensores y desplegar la data en la zona de código que dice:

    ```C++
    // lectura del sensor
    int data = analogRead( A0 );
    yield();

    // despliegue del dato leido
    Serial.print( "Data Analoga:" );
    Serial.print( data );
    Serial.println();
    yield();

    ```
    ___

6. Envía el codigo al controlador (NodeMCU) y revisa la data utilizando el monitor serial

    ![](SerialMonitor1.jpg)

    ___

7. Ahora es el momento de enviar la data al broker MQTT. Para ello modifica el valor de MODO_MQTT a 1 en la zona de código que dice

    ```C++
    // control del código
    #define MODO_MQTT       1       // 0: modo testing, 1: modo MQTT

    ```
    ___

8. Envía el programa al controlador (NodeMCU) y observa el monitor Serial

    ![](SerialMonitor2.jpg)

    ___

9. El dato de tu sensor está siendo enviado al broker MQTT a través de las instrucciones que están en la zona de código que dice

	  ```C++
    // publicamos la data
    dtostrf( data, 1, 2, str );
    mqtt.publish( "ipst/test/sensor1/A0", str );
  	```
    ___

10. El identificador ```"ipst/test/sensor1/A0"``` debes utilizarlo en cualquier aplicación que desees utilizar para monitorear la data leída

    ![](MqttDash.jpg)

    ___





